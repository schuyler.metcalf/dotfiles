;; M-x package-install color-theme-sanityinc and powerline

(unless (package-installed-p 'color-theme-sanityinc-tomorrow)
  (package-refresh-contents)
  (package-install 'color-theme-sanityinc-tomorrow))

(unless (package-installed-p 'powerline)
  (package-refresh-contents)
  (package-install 'powerline))

(disable-theme 'zenburn)
(color-theme-sanityinc-tomorrow-eighties)
(powerline-center-theme)
(tool-bar-mode -1)

;;  disable menu bar but not line numbers
(setq prelude-minimalistic-ui t)
(global-nlinum-mode t)
