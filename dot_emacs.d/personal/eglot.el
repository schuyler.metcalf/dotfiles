;; Configuration for eglot package
(prelude-require-package 'eglot)
(require 'eglot)
(add-to-list 'eglot-server-programs '(elixir-mode "~/lib/elixir-ls-0.13.0/language_server.sh"))

(add-hook 'elixir-mode-hook 'eglot-ensure)

(define-key eglot-mode-map (kbd "C-c h") 'eglot-help-at-point)
(define-key eglot-mode-map (kbd "M-.") 'xref-find-definitions)
(define-key eglot-mode-map (kbd "M-RET") 'completion-at-point)
(define-key eglot-mode-map (kbd "M-<f6>") 'eglot-rename)
(define-key eglot-mode-map (kbd "M-?") 'xref-find-references)
