(prelude-require-package 'yasnippet)
(require 'yasnippet)

(setq yas-snippet-dirs '("~/.emacs.d/personal/snippets/"))
(yas-global-mode 1)
