(when (require 'beancount nil t)
  (add-to-list 'auto-mode-alist '("\\.beancount\\'" . beancount-mode))
  (add-hook 'beancount-mode-hook #'outline-minor-mode)
  )
