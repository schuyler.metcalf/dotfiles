# dotfiles

## Description
Collection of personal dotfiles, using [chezmoi](https://www.chezmoi.io/)

## Installation
0. Install chezmoi: [Instructions](https://www.chezmoi.io/docs/install/)
1. `chezmoi init git@gitlab.com:schuyler.metcalf/dotfiles.git`
2. `chezmoi diff`
3. `chezmoi apply -v`
